\chapter{Initialization}
\label{chap:Initialization}

\index{initialization|(}

When it comes to running \VTKm code, there are a few ways in which various facilities, such as logging and device connections, can be initialized.
The preferred method of initializing these features is to run the \vtkmcont{Initialize} function.
Although it is not strictly necessary to call \textidentifier{Initialize}, it is recommended to set up state and check for available devices.

\textidentifier{Initialize} can be called without any arguments, in which case \VTKm will be initialized with defaults.
But it can also optionally take the \textcode{argc}\index{argc} and \textcode{argv}\index{argv} arguments to the \textcode{main} function to parse some options that control the state of \VTKm.
\VTKm accepts arguments that, for example, configure the compute device to use or establish logging levels.
Any arguments that are handled by \VTKm are removed from the \textcode{argc}/\textcode{argv} list so that your program can then respond to the remaining arguments.

\textidentifier{Initialize} takes an optional third argument that specifies some options on the behavior of the argument parsing.
The options are specified as a bit-wise ``or'' of fields specified in the \vtkmcont{InitializeOptions} enum.
The available initialize options are
\begin{description}
\item[\classmember*{InitializeOptions}{None}]
  Placeholder used when no options are enabled.
  This is the value used when the third argument to \textidentifier{Initialize} is not provided.
\item[\classmember*{InitializeOptions}{RequireDevice}]
  Issue an error if the device argument is not specified.
\item[\classmember*{InitializeOptions}{DefaultAnyDevice}]
  If no device is specified, treat it as if the user gave ``-{}-device=Any''.
  This means that \textidentifier{DeviceAdapterTagUndefined} will never be return in the result.
\item[\classmember*{InitializeOptions}{AddHelp}]
  Add a help option.
  If ``-h'' or ``-{}-help'' is provided, prints a usage statement.
  Of course, the usage statement will only print out arguments processed by \VTKm, which is why help is not given by default.
  A string with usage help is returned from \textidentifier{Initialize} so that the calling program can provide \VTKm's help in its own usage statement.
\item[\classmember*{InitializeOptions}{ErrorOnBadOption}]
  If an unknown option is encountered, the program terminates with an error.
  If this option is not provided, any unknown options are returned in \textcode{argv}.
  If this option is used, it is a good idea to use \classmember*{InitializeOptions}{AddHelp} as well.
\item[\classmember*{InitializeOptions}{ErrorOnBadArgument}]
  If an extra argument is encountered, the program terminates with an error.
  If this option is not provided, any unknown arguments are returned in \textcode{argv}.
\item[\classmember*{InitializeOptions}{Strict}]
  If supplied, Initialize treats its own arguments as the only ones supported by the application and provides an error if not followed exactly.
  This is a convenience option that is a combination of \classmember*{InitializeOptions}{ErrorOnBadOption}, \classmember*{InitializeOptions}{ErrorOnBadArgument}, and \classmember*{InitializeOptions}{AddHelp}.
\end{description}

As stated earlier, \vtkmcont{Initialize} removes parsed options from the \textcode{argc}/\textcode{argv} passed to it so that the calling program can further respond to command line arguments.
Additionally, \textidentifier{Initialize} returns an \vtkmcont{InitializeResult} object that contains the following information.

\begin{description}
\item[\classmember*{InitializeResult}{Device}]
  A \vtkmcont{DeviceAdapterId} that represents the device specified by the command line arguments.
  (See Chapter \ref{chap:ManagingDevices} for details on how \VTKm represents devices.)
  If no device is specified in the command line options, \vtkmcont{DeviceAdapterTagUndefined} is returned (unless the \classmember*{InitializeOptions}{DefaultAnyDevice} option is given, in which case \vtkmcont{DeviceAdapterTagAny} is returned).
\item[\classmember*{InitializeResult}{}]
\end{description}

\vtkmlisting{Calling \textidentifier{Initialize}.}{BasicInitialize.cxx}

\index{initialization|)}
